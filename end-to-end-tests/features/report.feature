Feature: Reporting

  Scenario: Manager asks for report
    Given two customers exist and are registered with DTU Pay
    And two merchants exist and are registered with DTU Pay
    And a series of transactions have happened between the accounts
    When the manager asks for all reports
    Then a list of reports are returned
    And the list contains all payments made

  Scenario: Customer asks for report
    Given two customers exist and are registered with DTU Pay
    And two merchants exist and are registered with DTU Pay
    And a series of transactions have happened between the accounts
    When one of the customers asks for reports
    Then a list of reports are returned
    And the list only contains payments that the customer was a part of

  Scenario: Merchant asks for report
    Given two customers exist and are registered with DTU Pay
    And two merchants exist and are registered with DTU Pay
    And a series of transactions have happened between the accounts
    When one of the merchants asks for reports
    Then a list of transactions are returned
    And the list only contains payments that the merchant was a part of