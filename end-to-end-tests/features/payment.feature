Feature: Payment

  Scenario: Successful Payment
    Given a customer with a bank account with balance 1000
    And the customer is registered with DTU Pay
    And the customer has a valid token
    Given a merchant with a bank account with balance 2000
    And the merchant is registered with DTU Pay
    When the merchant initiates a payment for 100 kr by the customer
    Then the payment is successful
    And the balance of the customer at the bank is 900 kr
    And the balance of the merchant at the bank is 2100 kr
    And the payment is recorded in DTU Pay

  Scenario: Unsuccessful payment, because of insufficient funds
    Given a customer with a bank account with balance 90
    And the customer is registered with DTU Pay
    And the customer has a valid token
    Given a merchant with a bank account with balance 2000
    And the merchant is registered with DTU Pay
    When the merchant initiates a payment for 100 kr by the customer
    Then the payment is unsuccessful
    And the merchant is notified with the message "Payment declined"
    And the balance of the customer at the bank is 90 kr
    And the balance of the merchant at the bank is 2000 kr

  Scenario: Customer reuses token
    Given a customer with a bank account with balance 1000
    And the customer is registered with DTU Pay
    And the customer has a valid token
    Given a merchant with a bank account with balance 2000
    And the merchant is registered with DTU Pay
    When the merchant initiates a payment for 100 kr by the customer
    Then the payment is successful
    When the merchant initiates a payment for 100 kr by the customer
    Then the payment is unsuccessful