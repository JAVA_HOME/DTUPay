Feature: Account

  Scenario: Customer created successfully
    Given the user is not registered in DTUPay
    And the user has a bank account
    When the user registers at DTUPay as a customer
    Then the user is registered as a customer in DTUPay

  Scenario: Merchant created successfully
    Given the user is not registered in DTUPay
    And the user has a bank account
    When the user registers at DTUPay as a merchant
    Then the user is registered as a merchant in DTUPay

  Scenario: Customer tries to register to DTUPay, but does not have a bank account
    Given the user is not registered in DTUPay
    And the user does not have a bank account
    When the user tries to register at DTUPay as a customer
    Then the user is not registered as a customer in DTUPay
    And And an error message is returned saying "Customer does not have bank account"

  Scenario: Merchant tries to register to DTUPay, but does not have a bank account
    Given the user is not registered in DTUPay
    And the user does not have a bank account
    When the user tries to register at DTUPay as a merchant
    Then the user is not registered as a merchant in DTUPay
    And And an error message is returned saying "Merchant does not have bank account"

  Scenario: Customer deregisters
    Given a customer is registered in DTUPay
    When the customer tries to deregister
    Then the customer is no longer registered in DTUPay

  Scenario: Merchant deregisters
    Given a merchant is registered in DTUPay
    When the merchant tries to deregister
    Then the merchant is no longer registered in DTUPay