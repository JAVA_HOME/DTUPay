package dtu.five;

import dtu.five.dto.Account;
import dtu.ws.fastmoney.User;

public class StepsHelper {

    /**
     * @author s204452
     */
    public User convertPersonToUser(Account account){
        User user = new User();
        user.setCprNumber(account.getCpr());
        user.setFirstName(account.getFirstName());
        user.setLastName(account.getLastName());
        return user;
    }
}

