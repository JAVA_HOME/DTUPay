package dtu.five;

import dtu.five.dto.Account;
import dtu.five.dto.*;
import dtu.five.dto.Transaction;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class MerchantAPI {

    WebTarget baseUrl;

    public MerchantAPI() {
        Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:8080");
    }

    /**
     * @author s204452
     */
    public Account register(Account account) throws AccountRegisterFailedException {
        int okStatus = 200;
        Response response = baseUrl.path("/merchants")
                .request()
                .post(Entity.entity(account, MediaType.APPLICATION_JSON));
        if (response.getStatus() != okStatus) {
            String errorMessage = response.readEntity(String.class);
            throw new AccountRegisterFailedException(errorMessage);
        }
        return response.readEntity(Account.class);
    }

    /**
     * @author s204197
     */
    public Optional<Transaction> pay(Transaction transaction) throws PaymentFailedException {
        int statusOk = 200;
        Response response = baseUrl.path("/merchants").path("/pay")
                .request()
                .post(Entity.entity(transaction, MediaType.APPLICATION_JSON));
        if (response.getStatus() != statusOk) {
            String errorMessage = response.readEntity(String.class);
            throw new PaymentFailedException(errorMessage);
        }
        return response.readEntity(Optional.class);
    }

    /**
     * @author s183816
     */
    public List<Transaction> getReports(UUID merchantId) {
        return baseUrl.path("/merchants/" + merchantId)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<Transaction>>(){});
    }

    /**
     * @author s204197
     */
    public Response deregister(Account account) {
        return baseUrl.path("/merchants/" + account.getId())
                .request()
                .delete();
    }
}
