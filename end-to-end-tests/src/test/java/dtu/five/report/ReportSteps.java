package dtu.five.report;

import dtu.five.CustomerAPI;
import dtu.five.ManagerAPI;
import dtu.five.MerchantAPI;
import dtu.five.StepsHelper;
import dtu.five.dto.Account;
import dtu.five.dto.*;
import dtu.five.dto.Transaction;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ReportSteps {

    private BankService bankService;
    private Account customer1, customer2, merchant1, merchant2;
    private List<Token> tokens1, tokens2;
    private List<Report> reports;
    private List<Transaction> transactions;
    private CustomerAPI customerAPI;
    private MerchantAPI merchantAPI;
    private ManagerAPI manager;
    private StepsHelper stepsHelper;

    /**
     * @author s204166
     */
    public ReportSteps(StepsHelper stepsHelper, CustomerAPI customerAPI, MerchantAPI merchantAPI, ManagerAPI manager) {
        this.bankService = new BankServiceService().getBankServicePort();
        this.customerAPI = customerAPI;
        this.merchantAPI = merchantAPI;
        this.manager = manager;
        this.stepsHelper = stepsHelper;
    }

    /**
     * @author s204166
     */
    @After
    public void cleanUp() throws BankServiceException_Exception {
        if (customer1 != null && customer1.getBankAccountNumber() != null) {
            bankService.retireAccount(customer1.getBankAccountNumber());
        }
        if (customer2 != null && customer2.getBankAccountNumber() != null) {
            bankService.retireAccount(customer2.getBankAccountNumber());
        }
        if (merchant1 != null && merchant1.getBankAccountNumber() != null) {
            bankService.retireAccount(merchant1.getBankAccountNumber());
        }
        if (merchant2 != null && merchant2.getBankAccountNumber() != null) {
            bankService.retireAccount(merchant2.getBankAccountNumber());
        }
    }

    /**
     * @author s204166
     */
    @Given("two customers exist and are registered with DTU Pay")
    public void twoCustomersExistAndAreRegisteredWithDTUPay() throws BankServiceException_Exception, AccountRegisterFailedException {
        customer1 = new Account("Joachim H.","Mikkelsen", "030265-1981");
        String bankAccountNumber1 = bankService.createAccountWithBalance(
                stepsHelper.convertPersonToUser(customer1)
                , new BigDecimal(1000)
        );
        this.customer1.setBankAccountNumber(bankAccountNumber1);
        this.customer1 = customerAPI.register(this.customer1);
        this.tokens1 = this.customerAPI.getTokens(2, customer1);

        customer2 = new Account("Asta M.","Lorenzen", "130565-0214");
        String bankAccountNumber2 = bankService.createAccountWithBalance(
                stepsHelper.convertPersonToUser(customer2)
                , new BigDecimal(1000)
        );
        this.customer2.setBankAccountNumber(bankAccountNumber2);
        this.customer2 = customerAPI.register(this.customer2);
        this.tokens2 = this.customerAPI.getTokens(2, customer2);
    }

    /**
     * @author s204166
     */
    @Given("two merchants exist and are registered with DTU Pay")
    public void twoMerchantsExistAndAreRegisteredWithDTUPay() throws BankServiceException_Exception, AccountRegisterFailedException {
        merchant1 = new Account("Nansen N.","Kjær", "170151-1359");
        String bankAccountNumber1 = bankService.createAccountWithBalance(
                stepsHelper.convertPersonToUser(merchant1)
                , new BigDecimal(2000)
        );
        this.merchant1.setBankAccountNumber(bankAccountNumber1);
        this.merchant1 = merchantAPI.register(this.merchant1);

        merchant2 = new Account("Laura A.","Laursen", "150567-2336");
        String bankAccountNumber2 = bankService.createAccountWithBalance(
                stepsHelper.convertPersonToUser(merchant2)
                , new BigDecimal(2000)
        );
        this.merchant2.setBankAccountNumber(bankAccountNumber2);
        this.merchant2 = merchantAPI.register(this.merchant2);
    }

    /**
     * @author s204166
     */
    @Given("a series of transactions have happened between the accounts")
    public void aSeriesOfTransactionsHaveHappenedBetweenTheAccounts() throws PaymentFailedException {
        Transaction transaction1 = new Transaction(tokens1.get(0), merchant1.getId(), 100);
        assertNotNull(merchantAPI.pay(transaction1));
        Transaction transaction2 = new Transaction(tokens1.get(1), merchant2.getId(), 100);
        assertNotNull(merchantAPI.pay(transaction2));
        Transaction transaction3 = new Transaction(tokens2.get(0), merchant1.getId(), 100);
        assertNotNull(merchantAPI.pay(transaction3));
        Transaction transaction4 = new Transaction(tokens2.get(1), merchant2.getId(), 100);
        assertNotNull(merchantAPI.pay(transaction4));
    }

    /**
     * @author s204166
     */
    @When("the manager asks for all reports")
    public void theManagerAsksForAllReports() {
        reports = manager.getReports();
    }

    /**
     * @author s204166
     */
    @Then("a list of reports are returned")
    public void aListOfReportsAreReturned() {
        assertNotNull(reports);
    }

    /**
     * @author s183816
     */
    @Then("the list contains all payments made")
    public void theListContainsAllPaymentsMade() {
        Report report1 = new Report(customer1.getId(),tokens1.get(0),merchant1.getId(),100);
        Report report2 = new Report(customer1.getId(),tokens1.get(1),merchant2.getId(),100);
        Report report3 = new Report(customer2.getId(),tokens2.get(0),merchant1.getId(),100);
        Report report4 = new Report(customer2.getId(),tokens2.get(1),merchant2.getId(),100);

        assertTrue(reports.contains(report1));
        assertTrue(reports.contains(report2));
        assertTrue(reports.contains(report3));
        assertTrue(reports.contains(report4));
    }

    /**
     * @author s183816
     */
    @When("one of the customers asks for reports")
    public void oneOfTheCustomersAsksForReports() {
        reports = customerAPI.getReport(customer1.getId());
    }
    @Then("the list only contains payments that the customer was a part of")
    public void theListOnlyContainsPaymentsThatTheCustomerWasAPartOf() {
        assertTrue(reports.size() == 2);

        for(Report report : reports) {
            assertTrue(report.getCustomerId().equals(customer1.getId()));
        }
    }

    /**
     * @author s183816
     */
    @When("one of the merchants asks for reports")
    public void oneOfTheMerchantsAsksForReports() {
        transactions = merchantAPI.getReports(merchant1.getId());
    }

    /**
     * @author s183816
     */
    @Then("a list of transactions are returned")
    public void aListOfTransactionsAreReturned() {
        assertNotNull(transactions);
    }

    /**
     * @author s183816
     */
    @Then("the list only contains payments that the merchant was a part of")
    public void theListOnlyContainsPaymentsThatTheMerchantWasAPartOf() {
        assertTrue(transactions.size() == 2);

        for(Transaction transaction : transactions) {
            assertTrue(transaction.getMid().equals(merchant1.getId()));
        }
    }
}
