package dtu.five;

import dtu.five.dto.Account;
import dtu.five.dto.*;
import dtu.five.dto.Report;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.util.List;
import java.util.UUID;

public class CustomerAPI {
    WebTarget baseUrl;

    public CustomerAPI() {
        Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:8080");
    }

    /**
     * @author s204423
     */
    public Account register(Account account) throws AccountRegisterFailedException {
        int okStatus = 200;
        Response response = baseUrl.path("/customers")
                .request()
                .post(Entity.entity(account, MediaType.APPLICATION_JSON));
        if (response.getStatus() != okStatus) {
            String errorMessage = response.readEntity(String.class);
            throw new AccountRegisterFailedException(errorMessage);
        }
        return response.readEntity(Account.class);
    }

    /**
     * @author s204423
     */
    public Response deregister(Account account) {
        return baseUrl.path("/customers/" + account.getId())
                .request()
                .delete();
    }

    /**
     * @author s204466
     */
    public List<Token> getTokens(int amount, Account customer) {
        return baseUrl.path("/customers/" + customer.getId())
                .request()
                .post(Entity.entity(amount, MediaType.APPLICATION_JSON), new GenericType<>() {});
    }

    /**
     * @author s204466
     */
    public List<Report> getReport(UUID customerId) {
        return baseUrl.path("/customers/" + customerId)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<Report>>(){});
    }


}
