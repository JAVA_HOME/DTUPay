package dtu.five.payment;

import dtu.five.CustomerAPI;
import dtu.five.ManagerAPI;
import dtu.five.MerchantAPI;
import dtu.five.StepsHelper;
import dtu.five.dto.Account;
import dtu.five.dto.*;
import dtu.five.dto.Report;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.*;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class PaymentSteps {
    private BankService bankService;
    private int amount;
    private Account customer, merchant;
    private Optional<Transaction> transactionResponse;
    private String errorMessage;
    private List<Token> tokens;
    private StepsHelper stepsHelper;
    private CustomerAPI customerAPI;
    private MerchantAPI merchantAPI;
    private ManagerAPI manager;


    /**
     * @author s183816
     */
    public PaymentSteps(StepsHelper stepsHelper, CustomerAPI customerAPI, MerchantAPI merchantAPI, ManagerAPI manager) {
        this.bankService = new BankServiceService().getBankServicePort();
        this.stepsHelper = stepsHelper;
        this.customerAPI = customerAPI;
        this.merchantAPI = merchantAPI;
        this.manager = manager;
    }

    /**
     * @author s183816
     */
    @After
    public void cleanUp() throws BankServiceException_Exception {
        if (customer != null && customer.getBankAccountNumber() != null) {
            bankService.retireAccount(customer.getBankAccountNumber());
        }
        if (merchant != null && merchant.getBankAccountNumber() != null) {
            bankService.retireAccount(merchant.getBankAccountNumber());
        }
        transactionResponse = Optional.empty();
    }

    /**
     * @author s183816
     */
    @Given("a customer with a bank account with balance {int}")
    public void aCustomerWithABankAccountWithBalance(Integer balance) throws BankServiceException_Exception {
        customer = new Account("Laura JJ.","SchultJz", "261003-6345");

        String bankAccountNumber = bankService.createAccountWithBalance(
                stepsHelper.convertPersonToUser(customer)
                , new BigDecimal(balance)
        );

        this.customer.setBankAccountNumber(bankAccountNumber);
    }

    /**
     * @author s183816
     */
    @Given("the customer is registered with DTU Pay")
    public void theCustomerIsRegisteredWithDTUPay() throws AccountRegisterFailedException {
        this.customer = customerAPI.register(this.customer);
        assertNotNull(this.customer.getId());
    }

    /**
     * @author s183816
     */
    @Given("the customer has a valid token")
    public void theCustomerHasAValidToken() {
        this.tokens = this.customerAPI.getTokens(3, customer);
    }

    /**
     * @author s183816
     */
    @Given("a merchant with a bank account with balance {int}")
    public void aMerchantWithABankAccountWithBalance(Integer balance) throws BankServiceException_Exception {
        merchant = new Account("Mie MM.","KroghH", "170554-0931");

        String bankAccountNumber = bankService.createAccountWithBalance(
                stepsHelper.convertPersonToUser(merchant)
                , new BigDecimal(balance)
        );

        this.merchant.setBankAccountNumber(bankAccountNumber);
    }

    /**
     * @author s204466
     */
    @Given("the merchant is registered with DTU Pay")
    public void theMerchantIsRegisteredWithDTUPay() throws AccountRegisterFailedException {
        this.merchant = merchantAPI.register(this.merchant);
        assertNotNull(this.merchant.getId());
    }

    /**
     * @author s204466
     */
    @When("the merchant initiates a payment for {int} kr by the customer")
    public void theMerchantInitiatesAPaymentForKrByTheCustomer(Integer amount) {
        this.amount = amount;
        Transaction transaction = new Transaction(tokens.get(0), merchant.getId(), amount);
        try {
            transactionResponse = merchantAPI.pay(transaction);
        } catch (PaymentFailedException error) {
            this.errorMessage = error.getMessage();
            this.transactionResponse = Optional.empty();
        }
    }

    /**
     * @author s204466
     */
    @Then("the payment is successful")
    public void thePaymentIsSuccessful() {
        assertFalse(transactionResponse.isEmpty());
    }

    /**
     * @author s204452
     */
    @Then("the balance of the customer at the bank is {int} kr")
    public void theBalanceOfTheCustomerAtTheBankIsKr(Integer balance) throws BankServiceException_Exception {
        assertEquals(new BigDecimal(balance),bankService.getAccount(customer.getBankAccountNumber()).getBalance());
    }

    /**
     * @author s204452
     */
    @Then("the balance of the merchant at the bank is {int} kr")
    public void theBalanceOfTheMerchantAtTheBankIsKr(Integer balance) throws BankServiceException_Exception {
        assertEquals(new BigDecimal(balance),bankService.getAccount(merchant.getBankAccountNumber()).getBalance());
    }

    /**
     * @author s204423
     */
    @Then("the payment is recorded in DTU Pay")
    public void thePaymentIsRecordedInDTUPay() {
        Report expected = new Report(customer.getId(),tokens.get(0),merchant.getId(),amount);
        assertTrue(manager.getReports().contains(expected));
    }

    /**
     * @author s204423
     */
    @Then("the payment is unsuccessful")
    public void thePaymentIsUnsuccessful() {
        assertTrue(transactionResponse.isEmpty());
    }

    /**
     * @author s204423
     */
    @Then("the merchant is notified with the message {string}")
    public void theMerchantIsNotifiedWithTheMessage(String errorMessage) {
        assertEquals(this.errorMessage, errorMessage);
    }
}