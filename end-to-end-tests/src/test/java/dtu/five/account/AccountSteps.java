package dtu.five.account;

import dtu.five.CustomerAPI;
import dtu.five.MerchantAPI;
import dtu.five.dto.Account;
import dtu.five.dto.AccountRegisterFailedException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.*;

public class AccountSteps {
    private CustomerAPI customerAPI;
    private MerchantAPI merchantAPI;
    private Account user;
    private int deregisterStatusCode;
    private String errorMessage;
    private int okStatus;

    /**
     * @author s204166
     */
    public AccountSteps(CustomerAPI customerAPI, MerchantAPI merchantAPI) {
        this.customerAPI = customerAPI;
        this.merchantAPI = merchantAPI;
    }

    /**
     * @author s204166
     */
    @Given("the user is not registered in DTUPay")
    public void theUserIsNotRegisteredInDTUPay() {
        this.user = new Account("Sebastian H.", "Thygesen", "180848-3137");
        assertNull(this.user.getId());
    }

    /**
     * @author s204166
     */
    @Given("the user has a bank account")
    public void theUserHasABankAccount() {
        this.user.setBankAccountNumber("5473 8180 3783 9372");
    }

    /**
     * @author s204166
     */
    @When("the user registers at DTUPay as a customer")
    public void theUserRegistersAtDTUPayAsACustomer() throws AccountRegisterFailedException {
        this.user = this.customerAPI.register(this.user);
    }

    /**
     * @author s204166
     */
    @Then("the user is registered as a customer in DTUPay")
    public void theUserIsRegisteredAsACustomerInDTUPay() {
        assertNotNull(this.user.getId());
    }

    /**
     * @author s204197
     */
    @When("the user registers at DTUPay as a merchant")
    public void theUserRegistersAtDTUPayAsAMerchant() throws AccountRegisterFailedException {
        this.user = this.merchantAPI.register(this.user);
    }

    /**
     * @author s204197
     */
    @Then("the user is registered as a merchant in DTUPay")
    public void theUserIsRegisteredAsAMerchantInDTUPay() {
        assertNotNull(this.user.getId());
    }

    /**
     * @author s204197
     */
    @Given("the user does not have a bank account")
    public void theUserDoesNotHaveABankAccount() {
        assertNull(this.user.getBankAccountNumber());
    }

    /**
     * @author s204197
     */
    @When("the user tries to register at DTUPay as a customer")
    public void theUserTriesToRegisterAtDTUPayAsACustomer() throws AccountRegisterFailedException {
        try {
            customerAPI.register(user);
        } catch(AccountRegisterFailedException e) {
            errorMessage = e.getMessage();
        }
    }

    /**
     * @author s204197
     */
    @Then("the user is not registered as a customer in DTUPay")
    public void theUserIsNotRegisteredAsACustomerInDTUPay() {
        assertNull(user.getId());
    }

    /**
     * @author s204197
     */
    @Then("And an error message is returned saying {string}")
    public void andAnErrorMessageIsReturnedSaying(String error) {
        assertEquals(error, errorMessage);
    }

    /**
     * @author s204423
     */
    @When("the user tries to register at DTUPay as a merchant")
    public void theUserTriesToRegisterAtDTUPayAsAMerchant() {
        try {
            merchantAPI.register(user);
        } catch(AccountRegisterFailedException e) {
            errorMessage = e.getMessage();
        }
    }

    /**
     * @author s204423
     */
    @Then("the user is not registered as a merchant in DTUPay")
    public void theUserIsNotRegisteredAsAMerchantInDTUPay() {
        assertNull(user.getId());
    }

    /**
     * @author s204423
     */
    @Given("a customer is registered in DTUPay")
    public void aCustomerIsRegisteredInDTUPay() throws AccountRegisterFailedException {
        this.user = new Account("Sebastian H.", "Thygesen", "180848-3137");
        this.user.setBankAccountNumber("5473 8180 3783 9372");
        this.user = this.customerAPI.register(this.user);
    }

    /**
     * @author s204423
     */
    @When("the customer tries to deregister")
    public void theCustomerTriesToDeregister() {
        deregisterStatusCode = customerAPI.deregister(this.user).getStatus();
    }

    /**
     * @author s204423
     */
    @Then("the customer is no longer registered in DTUPay")
    public void theCustomerIsNoLongerRegisteredInDTUPay() {
        this.okStatus = 200;
        assertEquals(deregisterStatusCode, okStatus);
    }

    /**
     * @author s204197
     */
    @Given("a merchant is registered in DTUPay")
    public void aMerchantIsRegisteredInDTUPay() throws AccountRegisterFailedException {
        this.user = new Account("Sebastian H.", "Thygesen", "180848-3137");
        this.user.setBankAccountNumber("5473 8180 3783 9372");
        this.user = this.merchantAPI.register(this.user);
    }
    /**
     * @author s204197
     */
    @When("the merchant tries to deregister")
    public void theMerchantTriesToDeregister() {
        deregisterStatusCode = merchantAPI.deregister(this.user).getStatus();
    }

    /**
     * @author s204197
     */
    @Then("the merchant is no longer registered in DTUPay")
    public void theMerchantIsNoLongerRegisteredInDTUPay() {
        this.okStatus = 200;
        assertEquals(deregisterStatusCode, okStatus);
    }
}
