package dtu.five.dto;

public class PaymentFailedException extends Exception {
    public PaymentFailedException(String s) {
        super(s);
    }
}
