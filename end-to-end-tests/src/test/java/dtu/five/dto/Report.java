package dtu.five.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data // Automatic getter and setters and equals etc
@NoArgsConstructor
@AllArgsConstructor
public class Report extends Transaction {

    private UUID customerId;

    public Report(UUID cid, Token token, UUID mid, int amount) {
        super(token, mid, amount);
        this.customerId = cid;
    }
}
