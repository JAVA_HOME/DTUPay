package dtu.five.dto;

public class AccountRegisterFailedException extends Exception {
    public AccountRegisterFailedException(String s) {
        super(s);
    }
}
