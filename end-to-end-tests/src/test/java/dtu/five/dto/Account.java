package dtu.five.dto;

import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@XmlRootElement // Needed for XML serialization and deserialization
@Data // Automatic getter and setters and equals etc
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    private String firstName;
    private String lastName;
    private String cpr;
    private String bankAccountNumber;
    private UUID id;

    public Account(String firstName, String lastName, String cpr){
        this.firstName = firstName;
        this.lastName = lastName;
        this.cpr = cpr;
    }
}