package dtu.five;

import dtu.five.dto.Report;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;

import java.util.List;

public class ManagerAPI {

    WebTarget baseUrl;

    public ManagerAPI() {
        Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:8080");
    }

    /**
     * @author s204166
     */
    public List<Report> getReports() {
        return baseUrl.path("/reports")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<Report>>(){});
    }
}
