Feature: Report Service Level Tests

  Scenario: Payment reported
    Given a transaction exist and is not present in the report service
    When a "SuccessfulPayment" event is received with the transaction
    Then the transaction is recorded as a report

  Scenario: Manager getting all reports
    When a "AllReportRequest" event is received
    Then a "AllReportsResponse" event is sent, containing the full list of reports

  Scenario: Customer requests reports
    Given two different customers have made a transaction, which has been recorded
    When a "CustomerReportRequest" event is received with the customer id of one of the customers
    Then a "CustomerReportResponse" event is sent, containing only the transaction made by the customer

  Scenario: Merchant requests reports
    Given two different merchants have made a transaction, which has been recorded
    When a "MerchantReportRequest" event is received with a merchant id of one of the merchants
    Then a "MerchantReportResponse" event is sent, containing only the transaction received by the merchant