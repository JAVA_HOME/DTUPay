package dtu.five.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class Token {
    private UUID id;

    public Token() {
        this.id = UUID.randomUUID();
    }
}
