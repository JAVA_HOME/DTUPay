package dtu.five.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data // Automatic getter and setters and equals etc
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {
    Token token;
    UUID mid;
    int amount;
}