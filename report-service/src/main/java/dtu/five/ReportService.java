package dtu.five;

import dtu.five.dto.Report;
import dtu.five.dto.Transaction;
import messaging.Event;
import messaging.MessageQueue;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ReportService {

    private MessageQueue queue;
    private List<Report> reportList;

    /**
     * @author s204166
     */
    public ReportService(MessageQueue queue) {
        this.queue = queue;
        this.reportList = new ArrayList<>();

        synchronized (this) {
            queue.addHandler("SuccessfulPayment",this::reportTransaction);
            queue.addHandler("AllReportRequest",this::getAllReports);
            queue.addHandler("CustomerReportRequest",this::getCustomerReports);
            queue.addHandler("MerchantReportRequest",this::getMerchantReports);
        }
    }

    /**
     * @author s204166
     */
    public void reportTransaction(Event event) {
        UUID customerId = event.getArgument(0, UUID.class);
        Transaction transaction = event.getArgument(1, Transaction.class);
        reportList.add(new Report(customerId,transaction.getToken(),transaction.getMid(),transaction.getAmount()));
    }

    /**
     * @author s204423
     */
    public void getAllReports(Event event) {
        UUID correlation = event.getArgument(0,UUID.class);
        Event response = new Event("AllReportsResponse", new Object[]{ reportList, correlation });
        queue.publish(response);
    }

    /**
     * @author s204423
     */
    public void getCustomerReports(Event event) {
        UUID customerId = event.getArgument(0, UUID.class);
        UUID correlation = event.getArgument(1, UUID.class);
        List<Report> result = new ArrayList<>();

        for(Report entry : reportList) {
            if(entry.getCustomerId().equals(customerId)) {
                result.add(entry);
            }
        }

        Event response = new Event("CustomerReportResponse", new Object[]{ result, correlation });
        queue.publish(response);
    }

    /**
     * @author s204423
     */
    public void getMerchantReports(Event event) {
        UUID merchantId = event.getArgument(0, UUID.class);
        UUID correlation = event.getArgument(1, UUID.class);
        List<Transaction> result = new ArrayList<>();

        for(Report entry : reportList) {
            if(entry.getMid().equals(merchantId)) {
                result.add(new Transaction(entry.getToken(),entry.getMid(),entry.getAmount()));
            }
        }

        Event response = new Event("MerchantReportResponse", new Object[]{ result, correlation });
        queue.publish(response);
    }

    public List<Report> getReportList() {
        return reportList;
    }
}
