package dtu.five;

import dtu.five.dto.Report;
import dtu.five.dto.Token;
import dtu.five.dto.Transaction;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ReportSteps {

    private MessageQueue queue = mock(MessageQueue.class);
    private ReportService service = new ReportService(queue);
    private Transaction transaction;
    private UUID customerId, merchantId, correlation;
    private Report report;
    private Event event;

    /**
     * @author s204466
     */
    @Given("a transaction exist and is not present in the report service")
    public void a_transaction_exist_and_is_not_present_in_the_report_service() {
        transaction = new Transaction(new Token(),UUID.randomUUID(),100);
        customerId = UUID.randomUUID();
        report = new Report(customerId,transaction.getToken(),transaction.getMid(),transaction.getAmount());
        assertFalse(service.getReportList().contains(report));
    }

    /**
     * @author s204466
     */
    @When("a {string} event is received with the transaction")
    public void a_event_is_received_with_the_transaction(String eventName) {
        event = new Event(eventName, new Object[]{ customerId, transaction });
        service.reportTransaction(event);
    }

    /**
     * @author s204466
     */
    @Then("the transaction is recorded as a report")
    public void the_transaction_is_recorded_as_a_report() {
        assertTrue(service.getReportList().contains(report));
    }

    /**
     * @author s204466
     */
    @When("a {string} event is received")
    public void a_event_is_received(String eventName) {
        correlation = UUID.randomUUID();
        event = new Event(eventName, new Object[]{ correlation });
        service.getAllReports(event);
    }

    /**
     * @author s204466
     */
    @Then("a {string} event is sent, containing the full list of reports")
    public void a_event_is_sent_containing_the_full_list_of_reports(String eventName) {
        event = new Event(eventName,new Object[]{ service.getReportList(), correlation });
        verify(queue).publish(event);
    }

    /**
     * @author s204423
     */
    @Given("two different customers have made a transaction, which has been recorded")
    public void two_different_customers_have_made_a_transaction_which_has_been_recorded() {
        customerId = UUID.randomUUID();
        transaction = new Transaction(new Token(),UUID.randomUUID(),100);
        event = new Event("SuccessfulPayment", new Object[]{ customerId, transaction });
        service.reportTransaction(event);
        event = new Event("SuccessfulPayment", new Object[]{ UUID.randomUUID(),new Transaction(new Token(),UUID.randomUUID(),200) });
        service.reportTransaction(event);
    }

    /**
     * @author s204423
     */
    @When("a {string} event is received with the customer id of one of the customers")
    public void a_event_is_received_with_the_customer_id_of_one_of_the_customers(String eventName) {
        correlation = UUID.randomUUID();
        event = new Event(eventName, new Object[]{ customerId, correlation });
        service.getCustomerReports(event);
    }

    /**
     * @author s204423
     */
    @Then("a {string} event is sent, containing only the transaction made by the customer")
    public void a_event_is_sent_containing_only_the_transaction_made_by_the_customer(String eventName) {
        List<Report> expected = new ArrayList<>();
        expected.add(new Report(customerId,transaction.getToken(),transaction.getMid(),transaction.getAmount()));
        event = new Event(eventName,new Object[]{ expected, correlation });
        verify(queue).publish(event);
    }

    /**
     * @author s204423
     */
    @Given("two different merchants have made a transaction, which has been recorded")
    public void two_different_merchants_have_made_a_transaction_which_has_been_recorded() {
        merchantId = UUID.randomUUID();
        transaction = new Transaction(new Token(),merchantId,100);
        event = new Event("SuccessfulPayment", new Object[]{ UUID.randomUUID(), transaction });
        service.reportTransaction(event);
        event = new Event("SuccessfulPayment", new Object[]{ UUID.randomUUID(), new Transaction(new Token(),UUID.randomUUID(),200) });
        service.reportTransaction(event);
    }

    /**
     * @author s204423
     */
    @When("a {string} event is received with a merchant id of one of the merchants")
    public void a_event_is_received_with_a_merchant_id_of_one_of_the_merchants(String eventName) {
        correlation = UUID.randomUUID();
        event = new Event(eventName, new Object[]{ merchantId, correlation });
        service.getMerchantReports(event);
    }

    /**
     * @author s204423
     */
    @Then("a {string} event is sent, containing only the transaction received by the merchant")
    public void a_event_is_sent_containing_only_the_transaction_received_by_the_merchant(String eventName) {
        List<Transaction> expected = new ArrayList<>();
        expected.add(transaction);
        event = new Event(eventName,new Object[]{ expected, correlation });
        verify(queue).publish(event);
    }
}
