
Feature: Customer Service Level Tests

  Scenario: Customer registers
    When a call to register a customer has been made by REST
    Then a "CustomerRegistrationRequested" event is sent containing the customer account
    And a "CustomerRegistered" event is received containing the updated customer account
    And the method returns an optional of the customer account

  Scenario: Customer deregisters
    When a call to deregister a customer has been made by REST
    Then a "CustomerDeregisterRequested" event is sent containing the customer id

  Scenario: Customer wants tokens
    When a call to get tokens is made by a customer via REST
    Then a "TokenCreationRequested" event is sent with the customers id and amount of tokens
    And a "GetTokenResponse" event is received with a list of tokens
    And the list of tokens is returned

  Scenario: Customer wants report
    When a call to get report is made by a customer via REST
    Then a "CustomerReportRequest" event is sent with the customers id
    And a "CustomerReportResponse" event is received with a list of reports
    And the list of reports is returned
