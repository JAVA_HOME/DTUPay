# @author s204452
Feature: Report Service Level Tests
  Scenario: Manager gets all reports in the system
    Given a report exist in the system
    When a call to get reports has been made by REST
    Then a "AllReportRequest" event is sent for report service
    When a "AllReportsResponse" event is received for report service
    Then return list contains the report



