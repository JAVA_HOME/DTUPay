
Feature: Merchant Service Level Tests
  Scenario: Merchant registers
    When a call to register a merchant has been made by REST
    Then a "MerchantRegistrationRequested" event is sent containing the merchant account
    And a "MerchantRegistered" event is received containing the updated merchant account
    And the method returns an optional of the merchant account

  Scenario: Merchant deregisters
    When a call to deregister a merchant has been made by REST
    Then a "MerchantDeregisterRequested" event is sent containing the merchant id

  Scenario: Merchant initiates payment with success
    When a call to pay has been made by a merchant using REST
    Then a "PaymentRequested" event is sent containing a transaction
    And a "SuccessfulPayment" event is received
    And the method returns the transaction

  Scenario: Merchant initiates payment without success
    When a call to pay has been made by a merchant using REST
    Then a "PaymentRequested" event is sent containing a transaction
    And a "PaymentFailed" event is received for failure
    And the result is empty

  Scenario: Merchant wants report
    When a call to get report is made by a merchant via REST
    Then a "MerchantReportRequest" event is sent with the merchant id
    And a "MerchantReportResponse" event is received for merchant with a list of reports
    And the list of reports is returned for merchant