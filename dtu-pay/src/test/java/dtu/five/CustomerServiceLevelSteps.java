package dtu.five;

import dtu.five.customer.CustomerService;
import dtu.five.dto.Account;
import dtu.five.dto.Report;
import dtu.five.dto.Token;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static java.lang.Thread.sleep;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class CustomerServiceLevelSteps {

    private MessageQueue queue = mock(MessageQueue.class);
    private CustomerService service = new CustomerService(queue);
    private Account account;
    private UUID customerId, correlationRegistration, correlationToken, correlationReport;
    private Optional<Account> resultRegister;
    private List<Token> resultToken;
    private List<Report> resultReport;
    private int tokenAmount;

    private CompletableFuture<Optional<Account>> register = new CompletableFuture<>();
    private CompletableFuture<List<Token>> token = new CompletableFuture<>();
    private CompletableFuture<List<Report>> report = new CompletableFuture<>();

    /**
     * @author s204466
     */
    @When("a call to register a customer has been made by REST")
    public void a_call_to_register_a_customer_has_been_made_by_rest() throws InterruptedException {
        account = new Account("firstname", "lastname", "cpr");
        account.setBankAccountNumber("bankAccNum");
        new Thread(() -> {
            resultRegister = service.register(account);
            register.complete(resultRegister);
        }).start();
        sleep(25);
    }

    /**
     * @author s204466
     */
    @Then("a {string} event is sent containing the customer account")
    public void a_event_is_sent_containing_the_customer_account(String eventName) {
        correlationRegistration = service.getCorrelationFromRegister();
        Event event = new Event(eventName,new Object[]{ account, correlationRegistration});
        verify(queue).publish(event);
    }

    /**
     * @author s204466
     */
    @Then("a {string} event is received containing the updated customer account")
    public void a_event_is_received_containing_the_updated_customer_account(String eventName) {
        Event event = new Event(eventName, new Object[]{ account, correlationRegistration});
        service.registerComplete(event);
    }

    /**
     * @author s204466
     */
    @Then("the method returns an optional of the customer account")
    public void the_method_returns_an_optional_of_the_customer_account() {
        assertFalse(register.join().isEmpty());
    }

    /**
     * @author s204423
     */
    @When("a call to deregister a customer has been made by REST")
    public void a_call_to_deregister_a_customer_has_been_made_by_rest() {
        customerId = UUID.randomUUID();
        service.deregister(customerId);
    }

    /**
     * @author s204423
     */
    @Then("a {string} event is sent containing the customer id")
    public void a_event_is_sent_containing_the_customer_id(String eventName) {
        Event expected = new Event(eventName, new Object[]{ customerId });
        verify(queue).publish(expected);
    }

    /**
     * @author s204423
     */
    @When("a call to get tokens is made by a customer via REST")
    public void a_call_to_get_tokens_is_made_by_a_customer_via_rest() throws InterruptedException {
        customerId = UUID.randomUUID();
        tokenAmount = 3;
        new Thread(() -> {
            resultToken = service.getTokens(tokenAmount,customerId);
            token.complete(resultToken);
        }).start();
        sleep(25);
    }

    /**
     * @author s204423
     */
    @Then("a {string} event is sent with the customers id and amount of tokens")
    public void a_event_is_sent_with_the_customers_id_and_amount_of_tokens(String eventName) {
        correlationToken = service.getCorrelationFromToken();
        Event event = new Event(eventName,new Object[]{ tokenAmount, customerId, correlationToken});
        verify(queue).publish(event);
    }

    /**
     * @author s204423
     */
    @Then("a {string} event is received with a list of tokens")
    public void a_event_is_received_with_a_list_of_tokens(String eventName) {
        List<Token> result = new ArrayList<>();
        result.add(new Token());
        result.add(new Token());
        result.add(new Token());

        Event event = new Event(eventName, new Object[]{ result, correlationToken});
        service.getTokenResponse(event);
    }

    /**
     * @author s204423
     */
    @Then("the list of tokens is returned")
    public void the_list_of_tokens_is_returned() {
        List<Token> result = token.join();
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }

    /**
     * @author s204466
     */
    @When("a call to get report is made by a customer via REST")
    public void a_call_to_get_report_is_made_by_a_customer_via_rest() throws InterruptedException {
        customerId = UUID.randomUUID();
        new Thread(() -> {
            resultReport = service.getReport(customerId);
            report.complete(resultReport);
        }).start();
        sleep(25);
    }

    /**
     * @author s204466
     */
    @Then("a {string} event is sent with the customers id")
    public void a_event_is_sent_with_the_customers_id(String eventName) {
        correlationReport = service.getCorrelationFromReport();
        Event event = new Event(eventName,new Object[]{ customerId, correlationReport});
        verify(queue).publish(event);
    }

    /**
     * @author s204466
     */
    @Then("a {string} event is received with a list of reports")
    public void a_event_is_received_with_a_list_of_reports(String eventName) {
        List<Report> result = new ArrayList<>();
        result.add(new Report());
        result.add(new Report());
        result.add(new Report());

        Event event = new Event(eventName,new Object[]{ result, correlationReport});
        service.getReportResponse(event);
    }

    /**
     * @author s204466
     */
    @Then("the list of reports is returned")
    public void the_list_of_reports_is_returned() {
        List<Report> result = report.join();
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }
}
