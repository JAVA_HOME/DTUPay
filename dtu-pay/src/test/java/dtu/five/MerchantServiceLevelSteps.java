package dtu.five;

import dtu.five.dto.Account;
import dtu.five.dto.Token;
import dtu.five.dto.Transaction;
import dtu.five.merchants.MerchantService;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static java.lang.Thread.sleep;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class MerchantServiceLevelSteps {
    private MessageQueue queue = mock(MessageQueue.class);
    private MerchantService service = new MerchantService(queue);
    private Account account;
    private Transaction transaction;
    private UUID merchantId, correlationRegistration, correlationPay, correlationReport;
    private Optional<Account> resultRegister;
    private List<Transaction> resultReport;
    private Optional<Transaction> resultPay;
    private CompletableFuture<Optional<Account>> register = new CompletableFuture<>();
    private CompletableFuture<Optional<Transaction>> pay = new CompletableFuture<>();
    private CompletableFuture<List<Transaction>> report = new CompletableFuture<>();

    /**
     * @author s204423
     */
    @When("a call to register a merchant has been made by REST")
    public void a_call_to_register_a_merchant_has_been_made_by_rest() throws InterruptedException {
        synchronized (this) {
            account = new Account("firstname", "lastname", "cpr");
            account.setBankAccountNumber("bankAccNum");
            new Thread(() -> {
                resultRegister = service.register(account);
                register.complete(resultRegister);
            }).start();
            sleep(25);
        }
    }

    /**
     * @author s204423
     */
    @Then("a {string} event is sent containing the merchant account")
    public void a_event_is_sent_containing_the_merchant_account(String eventName) {
        correlationRegistration = service.getCorrelationFromRegister();
        Event event = new Event(eventName,new Object[]{ account, correlationRegistration});
        verify(queue).publish(event);
    }

    /**
     * @author s204423
     */
    @Then("a {string} event is received containing the updated merchant account")
    public void a_event_is_received_containing_the_updated_merchant_account(String eventName) {
        Event event = new Event(eventName, new Object[]{ account, correlationRegistration});
        service.registerComplete(event);
    }

    /**
     * @author s204423
     */
    @Then("the method returns an optional of the merchant account")
    public void the_method_returns_an_optional_of_the_merchant_account() {
        assertFalse(register.join().isEmpty());
    }

    /**
     * @author s204466
     */
    @When("a call to deregister a merchant has been made by REST")
    public void a_call_to_deregister_a_merchant_has_been_made_by_rest() {
        merchantId = UUID.randomUUID();
        service.deregister(merchantId);
    }

    /**
     * @author s204466
     */
    @Then("a {string} event is sent containing the merchant id")
    public void a_event_is_sent_containing_the_merchant_id(String eventName) {
        Event expected = new Event(eventName, new Object[]{ merchantId });
        verify(queue).publish(expected);
    }

    /**
     * @author s183816
     */
    @When("a call to pay has been made by a merchant using REST")
    public void a_call_to_pay_has_been_made_by_a_merchant_using_rest() throws InterruptedException {
        transaction = new Transaction(new Token(), UUID.randomUUID(),200);
        new Thread(() -> {
            resultPay = service.pay(transaction);
            pay.complete(resultPay);
        }).start();
        sleep(25);
    }

    /**
     * @author s183816
     */
    @Then("a {string} event is sent containing a transaction")
    public void a_event_is_sent_containing_a_transaction(String eventName) {
        correlationPay = service.getCorrelationFromPayment();
        Event event = new Event(eventName,new Object[]{ transaction, correlationPay });
        verify(queue).publish(event);
    }

    /**
     * @author s183816
     */
    @Then("a {string} event is received")
    public void a_event_is_received(String eventName) {
        Event event = new Event(eventName,new Object[]{ 0, 0, correlationPay });
        service.paymentSuccessful(event);
    }

    /**
     * @author s183816
     */
    @Then("the method returns the transaction")
    public void the_method_returns_the_transaction() {
        assertEquals(pay.join().get(), transaction);
    }

    /**
     * @author s204452
     */
    @When("a call to get report is made by a merchant via REST")
    public void aCallToGetReportIsMadeByAMerchantViaREST() throws InterruptedException {
        merchantId = UUID.randomUUID();
        new Thread(() -> {
            resultReport = service.getReports(merchantId);
            report.complete(resultReport);
        }).start();
        sleep(25);
    }

    /**
     * @author s204452
     */
    @Then("a {string} event is sent with the merchant id")
    public void aEventIsSentWithTheMerchantId(String eventName) {
        correlationReport = service.getCorrelationFromReport();
        Event event = new Event(eventName,new Object[]{ merchantId, correlationReport});
        verify(queue).publish(event);
    }

    /**
     * @author s204452
     */
    @And("a {string} event is received for merchant with a list of reports")
    public void aEventIsReceivedForMerchantWithAListOfReports(String eventName) {
        List<Transaction> result = new ArrayList<>();
        result.add(new Transaction());
        result.add(new Transaction());
        result.add(new Transaction());

        Event event = new Event(eventName,new Object[]{ result, correlationReport});
        service.getReportResponse(event);
    }

    /**
     * @author s204452
     */
    @And("the list of reports is returned for merchant")
    public void theListOfReportsIsReturnedForMerchant() {
        List<Transaction> result = report.join();
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }

    /**
     * @author s204452
     */
    @And("the result is empty")
    public void theResultIsEmpty() {
        assertTrue(pay.join().isEmpty());
    }

    /**
     * @author s204423
     */
    @And("a {string} event is received for failure")
    public void aEventIsReceivedForFailure(String eventName) {
        Event event = new Event(eventName,new Object[]{ correlationPay});
        service.paymentFailed(event);
    }
}
