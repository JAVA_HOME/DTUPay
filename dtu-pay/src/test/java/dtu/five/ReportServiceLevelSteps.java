package dtu.five;

import dtu.five.dto.Report;
import dtu.five.dto.Token;
import dtu.five.report.ReportService;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;

import java.util.*;
import java.util.concurrent.CompletableFuture;


import static io.smallrye.common.constraint.Assert.assertTrue;
import static java.lang.Thread.sleep;
import static org.mockito.Mockito.*;

/**
 * @author s204452
 *
 */
public class ReportServiceLevelSteps {

    private MessageQueue queue;
    private ReportService service;

    private List<Report> reportList;

    private Report report;
    private UUID customerId;
    private UUID correlationReport;
    private CompletableFuture<List<Report>> reportFuture = new CompletableFuture<>();

    /**
     * @author s204452
     */
    public ReportServiceLevelSteps() {
        queue = mock(MessageQueue.class);
        service = new ReportService(queue);
    }

    /**
     * @author s204452
     */
    @Given("a report exist in the system")
    public void aReportExistInTheSystem() {
        customerId = UUID.randomUUID();
        report = new Report(customerId, new Token(), UUID.randomUUID(), 10);

    }

    /**
     * @author s204452
     */
    @When("a call to get reports has been made by REST")
    public void aCallToGetReportsHasBeenMadeByREST() throws InterruptedException {
        new Thread(() -> {
            reportList = service.getReports();
            reportFuture.complete(reportList);
        }).start();
        sleep(25);
    }

    /**
     * @author s204423
     */
    @Then("a {string} event is sent for report service")
    public void aEventIsSentForReportService(String eventName) {
        correlationReport = service.getCorrelationFromReport();
        Event event = new Event(eventName,new Object[]{correlationReport});
        verify(queue, timeout(300)).publish(event);
    }

    /**
     * @author s204423
     */
    @When("a {string} event is received for report service")
    public void aEventIsReceivedForReportService(String eventName) {
        Event event = new Event(eventName,new Object[]{ List.of(report), correlationReport });
        service.getReportsResponse(event);
    }

    /**
     * @author s204423
     */
    @Then("return list contains the report")
    public void returnListContainsTheReport() {
        assertTrue(reportFuture.join().size() > 0);
    }
}
