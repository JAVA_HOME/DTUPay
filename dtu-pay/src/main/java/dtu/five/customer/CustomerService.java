package dtu.five.customer;

import dtu.five.dto.Account;
import dtu.five.dto.Report;
import dtu.five.dto.Token;
import messaging.Event;
import messaging.MessageQueue;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

public class CustomerService {

    private MessageQueue queue;

    private Map<UUID,CompletableFuture<Account>> registeredPerson = new ConcurrentHashMap<>();
    private Map<UUID,CompletableFuture<List<Token>>> receivedTokens = new ConcurrentHashMap<>();
    private Map<UUID,CompletableFuture<List<Report>>> receivedReport = new ConcurrentHashMap<>();

    /**
     * @author s204423
     */
    public CustomerService(MessageQueue queue){
        this.queue = queue;

        synchronized (this) {
            queue.addHandler("CustomerRegistered",this::registerComplete);
            queue.addHandler("GetTokenResponse", this::getTokenResponse);
            queue.addHandler("CustomerReportResponse",this::getReportResponse);
        }
    }

    /**
     * @author s204423
     */
    public Optional<Account> register(Account account) {
        if(account.getBankAccountNumber() == null) {
            return Optional.empty();
        }
        UUID correlation = UUID.randomUUID();
        registeredPerson.put(correlation,new CompletableFuture<>());
        Event event = new Event("CustomerRegistrationRequested", new Object[] { account, correlation });
        queue.publish(event);
        Account response = registeredPerson.get(correlation).join();
        return Optional.of(response);
    }

    /**
     * @author s204423
     */
    public void registerComplete(Event event) {
        Account account = event.getArgument(0, Account.class);
        UUID correlation = event.getArgument(1,UUID.class);
        registeredPerson.get(correlation).complete(account);
    }

    /**
     * @author s204466
     */
    public void deregister(UUID id) {
        Event event = new Event("CustomerDeregisterRequested",new Object[]{ id });
        queue.publish(event);
    }

    /**
     * @author s204466
     */
    public List<Token> getTokens(int amount, UUID customerId) {
        UUID correlation = UUID.randomUUID();
        receivedTokens.put(correlation,new CompletableFuture<>());
        Event event = new Event("TokenCreationRequested", new Object[] { amount, customerId, correlation });
        queue.publish(event);
        return receivedTokens.get(correlation).join();
    }

    /**
     * @author s204466
     */
    public void getTokenResponse(Event event) {
        List<Token> tokens = event.getArgument(0, ArrayList.class);
        UUID correlation = event.getArgument(1, UUID.class);
        receivedTokens.get(correlation).complete(tokens);
    }

    /**
     * @author s204423
     */
    public List<Report> getReport(UUID id) {
        UUID correlation = UUID.randomUUID();
        receivedReport.put(correlation,new CompletableFuture<>());
        Event event = new Event("CustomerReportRequest",new Object[]{ id, correlation });
        queue.publish(event);
        return receivedReport.get(correlation).join();
    }

    /**
     * @author s204423
     */
    public void getReportResponse(Event event) {
        List<Report> report = event.getArgument(0, ArrayList.class);
        UUID correlation = event.getArgument(1, UUID.class);
        receivedReport.get(correlation).complete(report);
    }

    /**
     * @author s204423
     */
    public UUID getCorrelationFromRegister() {
        for(UUID cor : registeredPerson.keySet()) {
            if(!registeredPerson.get(cor).isDone()) {
                return cor;
            }
        }
        return null;
    }

    /**
     * @author s204423
     */
    public UUID getCorrelationFromToken() {
        for(UUID cor : receivedTokens.keySet()) {
            if(!receivedTokens.get(cor).isDone()) {
                return cor;
            }
        }
        return null;
    }

    /**
     * @author s204423
     */
    public UUID getCorrelationFromReport() {
        for(UUID cor : receivedReport.keySet()) {
            if(!receivedReport.get(cor).isDone()) {
                return cor;
            }
        }
        return null;
    }
}
