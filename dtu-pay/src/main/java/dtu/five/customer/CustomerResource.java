package dtu.five.customer;

import dtu.five.dto.Account;
import dtu.five.dto.Report;
import dtu.five.dto.Token;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Path("/customers")
public class CustomerResource {

    private CustomerService service = new CustomerServiceFactory().getService();


    /**
     * @author s204423
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response register(Account account) {
        Optional<Account> response = service.register(account);
        if (response.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .type(MediaType.TEXT_PLAIN)
                    .entity("Customer does not have bank account")
                    .build();
        }
        return Response.ok(response.get()).build();
    }

    /**
     * @author s204423
     */
    @DELETE
    @Path("/{id}")
    public Response deregister(@PathParam("id") UUID id) {
        service.deregister(id);
        return Response.ok().build();
    }

    /**
     * @author s204466
     */
    @POST
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Token> getTokens(int amount, @PathParam("id") UUID customerId) {
        return service.getTokens(amount,customerId);
    }

    /**
     * @author s204466
     */
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Report> getReport(@PathParam("id") UUID id) {
        return service.getReport(id);
    }

}
