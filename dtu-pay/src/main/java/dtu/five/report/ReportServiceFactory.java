package dtu.five.report;

import messaging.implementations.RabbitMqQueue;

public class ReportServiceFactory {

    static ReportService service = null;

    public synchronized ReportService getService() {
        if (service != null) {
            return service;
        }
        var mq = new RabbitMqQueue("rabbitMq");
        service = new ReportService(mq);
        return service;

    }
}
