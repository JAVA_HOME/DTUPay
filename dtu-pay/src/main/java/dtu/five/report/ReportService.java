package dtu.five.report;

import dtu.five.dto.Report;
import messaging.Event;
import messaging.MessageQueue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

public class ReportService {

    private Map<UUID, CompletableFuture<List<Report>>> receivedReports = new ConcurrentHashMap<>();

    MessageQueue queue;

    /**
     * @author s204166
     */
    public ReportService(MessageQueue mq){
        this.queue = mq;

        synchronized (this) {
            queue.addHandler("AllReportsResponse",this::getReportsResponse);
        }
    }

    /**
     * @author s204166
     */
    public void getReportsResponse(Event event) {
        List<Report> response = event.getArgument(0, ArrayList.class);
        UUID correlation = event.getArgument(1, UUID.class);
        receivedReports.get(correlation).complete(response);
    }

    /**
     * @author s204166
     */
    public List<Report> getReports() {
        UUID correlation = UUID.randomUUID();
        receivedReports.put(correlation,new CompletableFuture<>());
        Event event = new Event("AllReportRequest",new Object[]{ correlation });
        queue.publish(event);
        return receivedReports.get(correlation).join();
    }

    /**
     * @author s204166
     */
    public UUID getCorrelationFromReport() {
        for(UUID cor : receivedReports.keySet()) {
            if(!receivedReports.get(cor).isDone()) {
                return cor;
            }
        }
        return null;
    }

}
