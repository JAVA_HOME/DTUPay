package dtu.five.report;

import dtu.five.dto.Report;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import java.util.List;

@Path("/reports")
public class ReportResource {

    private ReportService service = new ReportServiceFactory().getService();

    /**
     * @author s204166
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Report> getReports() { return service.getReports(); }

}
