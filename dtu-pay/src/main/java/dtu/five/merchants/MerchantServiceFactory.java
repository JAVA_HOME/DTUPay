package dtu.five.merchants;


import messaging.implementations.RabbitMqQueue;

public class MerchantServiceFactory {

    static MerchantService service = null;

    public synchronized MerchantService getService() {
        if (service != null) {
            return service;
        }
        var mq = new RabbitMqQueue("rabbitMq");
        service = new MerchantService(mq);
        return service;

    }

}
