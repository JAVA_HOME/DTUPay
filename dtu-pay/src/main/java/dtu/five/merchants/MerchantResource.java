package dtu.five.merchants;

import dtu.five.dto.Account;
import dtu.five.dto.Transaction;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Path("/merchants")
public class MerchantResource {
    private MerchantService service = new MerchantServiceFactory().getService();

    /**
     * @author s204452
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response register(Account account) {
        Optional<Account> response = service.register(account);
        if (response.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .type(MediaType.TEXT_PLAIN)
                    .entity("Merchant does not have bank account")
                    .build();
        }
        return Response.ok(response.get()).build();
    }

    /**
     * @author s204197
     */
    @DELETE
    @Path("/{id}")
    public Response deregister(@PathParam("id") UUID id) {
        service.deregister(id);
        return Response.ok().build();
    }

    /**
     * @author s183816
     */
    @POST
    @Path("/pay")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response pay(Transaction transaction) {
        Optional<Transaction> transactionResponse = service.pay(transaction);
        if (transactionResponse.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .type(MediaType.TEXT_PLAIN)
                    .entity("Payment declined")
                    .build();
        }
        return Response.ok(transactionResponse).build();
    }

    /**
     * @author s204197
     */
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Transaction> getReports(@PathParam("id") UUID id) {
        return service.getReports(id);
    }

}
