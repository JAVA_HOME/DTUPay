package dtu.five.merchants;

import dtu.five.dto.Account;
import dtu.five.dto.Report;
import dtu.five.dto.Transaction;
import messaging.Event;
import messaging.MessageQueue;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

public class MerchantService {

    private MessageQueue queue;

    private Map<UUID, CompletableFuture<Account>> registeredPerson = new ConcurrentHashMap<>();
    private Map<UUID, CompletableFuture<Boolean>> paymentStatus = new ConcurrentHashMap<>();
    private Map<UUID,CompletableFuture<List<Transaction>>> receivedReport = new ConcurrentHashMap<>();

    /**
     * @author s183816
     */
    public MerchantService(MessageQueue queue) {
        this.queue = queue;

        synchronized (this) {
            queue.addHandler("MerchantRegistered",this::registerComplete);
            queue.addHandler("SuccessfulPayment",this::paymentSuccessful);
            queue.addHandler("PaymentFailed",this::paymentFailed);
            queue.addHandler("MerchantReportResponse",this::getReportResponse);
        }
    }

    /**
     * @author s204452
     */
    public Optional<Account> register(Account account) {
        if(account.getBankAccountNumber() == null) {
            return Optional.empty();
        }
        UUID correlation = UUID.randomUUID();
        registeredPerson.put(correlation,new CompletableFuture<>());
        Event event = new Event("MerchantRegistrationRequested", new Object[] {account, correlation });
        queue.publish(event);
        Account response = registeredPerson.get(correlation).join();
        return Optional.of(response);
    }

    /**
     * @author s204452
     */
    public void registerComplete(Event event) {
        Account account = event.getArgument(0, Account.class);
        UUID correlation = event.getArgument(1,UUID.class);
        registeredPerson.get(correlation).complete(account);
    }

    /**
     * @author s204197
     */
    public void deregister(UUID id) {
        Event event = new Event("MerchantDeregisterRequested",new Object[]{ id });
        queue.publish(event);
    }

    /**
     * @author s183816
     */
    public Optional<Transaction> pay(Transaction transaction) {
        UUID correlation = UUID.randomUUID();
        paymentStatus.put(correlation,new CompletableFuture<>());
        Event event = new Event("PaymentRequested", new Object[] { transaction, correlation });
        queue.publish(event);
        if (paymentStatus.get(correlation).join()) {
            return Optional.of(transaction);
        }
        return Optional.empty();
    }

    /**
     * @author s183816
     */
    public void paymentSuccessful(Event event) {
        UUID correlation = event.getArgument(2,UUID.class);
        paymentStatus.get(correlation).complete(true);
    }

    /**
     * @author s183816
     */
    public void paymentFailed(Event event) {
        UUID correlation = event.getArgument(0,UUID.class);
        paymentStatus.get(correlation).complete(false);
    }

    /**
     * @author s204197
     */
    public List<Transaction> getReports(UUID id) {
        UUID correlation = UUID.randomUUID();
        receivedReport.put(correlation,new CompletableFuture<>());
        Event event = new Event("MerchantReportRequest",new Object[]{ id, correlation });
        queue.publish(event);
        return receivedReport.get(correlation).join();
    }

    /**
     * @author s204197
     */
    public void getReportResponse(Event event) {
        List<Transaction> report = event.getArgument(0, ArrayList.class);
        UUID correlation = event.getArgument(1, UUID.class);
        receivedReport.get(correlation).complete(report);
    }

    /**
     * @author s204423
     */
    public UUID getCorrelationFromRegister() {
        for(UUID cor : registeredPerson.keySet()) {
            if(!registeredPerson.get(cor).isDone()) {
                return cor;
            }
        }
        return null;
    }

    /**
     * @author s204423
     */
    public UUID getCorrelationFromPayment() {
        for(UUID cor : paymentStatus.keySet()) {
            if(!paymentStatus.get(cor).isDone()) {
                return cor;
            }
        }
        return null;
    }

    /**
     * @author s204423
     */
    public UUID getCorrelationFromReport() {
        for(UUID cor : receivedReport.keySet()) {
            if(!receivedReport.get(cor).isDone()) {
                return cor;
            }
        }
        return null;
    }
}
