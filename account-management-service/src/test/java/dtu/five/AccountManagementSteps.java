package dtu.five;

import dtu.five.dto.Account;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class AccountManagementSteps {
    private Account customer, merchant;
    private String customerBankNumber, merchantBankNumber;
    private List<String> bankIds;
    private MessageQueue messageQueue = mock(MessageQueue.class);
    private AccountManagementService accountManagementService= new AccountManagementService(messageQueue);
    private UUID correlation;

    /**
     * @author s204197
     */
    @Given("customer is not registered")
    public void customerIsNotRegistered() {
        this.customer = new Account("Andreas A.", "Schultz","230843-4551" );
    }

    /**
     * @author s204197
     */
    @When("an event {string} is sent by customer")
    public void anEventIsSentByCustomer(String eventName) {
        this.correlation = UUID.randomUUID();
        Event event = new Event(eventName, new Object[]{customer, correlation});
        this.customer = accountManagementService.createCustomer(event);
    }

    /**
     * @author s204197
     */
    @Then("a new event {string} is sent and then the customer is registered")
    public void aNewEventIsSentAndThenTheCustomerIsRegistered(String eventName) {
        Event event = new Event(eventName, new Object[]{customer, correlation});
        verify(messageQueue).publish(event);
    }

    /**
     * @author s204166
     */
    @Given("merchant is not registered")
    public void merchantIsNotRegistered() {
        this.merchant = new Account("Hjalte N.", "Mogensen", "081242-1557 ");
    }

    /**
     * @author s204166
     */
    @When("an event {string} is sent by merchant")
    public void anEventIsSentByMerchant(String eventName) {
        this.correlation = UUID.randomUUID();
        Event event = new Event(eventName, new Object[]{merchant, correlation});
        this.merchant = accountManagementService.createMerchant(event);
    }

    /**
     * @author s204166
     */
    @Then("a new event {string} is sent and then the merchant is registered")
    public void aNewEventIsSentAndThenTheMerchantIsRegistered(String eventName) {
        Event event = new Event(eventName, new Object[]{merchant, correlation});
        verify(messageQueue).publish(event);
    }

    /**
     * @author s204197
     */
    @Given("there is a customer and a merchant in the system")
    public void there_is_a_customer_and_a_merchant_in_the_system() {
        this.customer = new Account("Søren M.", "Hansen","160789-0869");
        this.merchant = new Account("Hjalte L.", "Jakobsen", "141194-1805");
        this.correlation = UUID.randomUUID();
        Event eventCustomer = new Event("CustomerRegistrationRequested", new Object[]{customer, correlation});
        this.customer = accountManagementService.createCustomer(eventCustomer);
        this.correlation = UUID.randomUUID();
        Event eventMerchant = new Event("MerchantRegistrationRequested", new Object[]{merchant, correlation});
        this.merchant = accountManagementService.createMerchant(eventMerchant);
    }

    /**
     * @author s204197
     */
    @When("an event {string} is sent by the system")
    public void an_event_is_sent_by_the_system(String eventName) {
        this.correlation = UUID.randomUUID();
        Event event = new Event(eventName, new Object[]{customer.getId(), merchant.getId(), correlation});
        bankIds = accountManagementService.getBankAccountNumbers(event);
    }

    /**
     * @author s204197
     */
    @Then("a new event {string} is returned")
    public void a_new_event_is_returned(String eventName) {
        Event event = new Event(eventName, new Object[]{bankIds, correlation});
        verify(messageQueue).publish(event);
    }

    /**
     * @author s204197
     */
    @Given("user is registered")
    public void userIsRegistered() {
        this.customer = new Account("Lars", "Larsen","180402-4313");
        customerBankNumber = "12345";
        this.customer.setBankAccountNumber(customerBankNumber);
        this.correlation = UUID.randomUUID();
        Event customerEvent = new Event("CustomerRegistrationRequested", new Object[]{customer, correlation});
        this.customer = accountManagementService.createCustomer(customerEvent);

        this.merchant = new Account("Søren", "Sørensen","051022-3043");
        merchantBankNumber = "54321";
        this.merchant.setBankAccountNumber(merchantBankNumber);
        this.correlation = UUID.randomUUID();
        Event merchantEvent = new Event("MerchantRegistrationRequested", new Object[]{merchant, correlation});
        this.merchant = accountManagementService.createMerchant(merchantEvent);
    }

    /**
     * @author s204166
     */
    @When("an event {string} is sent by the customer")
    public void anEventIsSentByTheCustomer(String eventName) {
        Event deleteEvent = new Event(eventName, new Object[]{customer.getId()});
        accountManagementService.deleteCustomer(deleteEvent);
    }

    /**
     * @author s204166
     */
    @Then("the customer is deregistered")
    public void theCustomerIsDeregistered() {
        assertFalse(accountManagementService.getCustomers().contains(customer));
    }

    /**
     * @author s204166
     */
    @When("an event {string} is sent by the merchant")
    public void anEventIsSentByTheMerchant(String eventName) {
        Event deleteEvent = new Event(eventName, new Object[]{merchant.getId()});
        accountManagementService.deleteMerchant(deleteEvent);
    }

    /**
     * @author s204166
     */
    @Then("the merchant is deregistered")
    public void aNewEventIsSentAndThenTheMerchantIsDeregistered() {
        assertFalse(accountManagementService.getMerchants().contains(merchant));
    }

}