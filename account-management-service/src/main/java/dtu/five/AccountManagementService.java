package dtu.five;

import dtu.five.dto.Account;
import messaging.Event;
import messaging.MessageQueue;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class AccountManagementService {
    private MessageQueue queue;
    private List<Account> customers;
    private List<Account> merchants;

    /**
     * @author s204166
     */
    public AccountManagementService(MessageQueue queue) {
        this.queue = queue;
        this.customers = new ArrayList<>();
        this.merchants = new ArrayList<>();

        synchronized (this) {
            this.queue.addHandler("CustomerRegistrationRequested",this::createCustomer);
            this.queue.addHandler("CustomerDeregisterRequested",this::deleteCustomer);
            this.queue.addHandler("MerchantRegistrationRequested",this::createMerchant);
            this.queue.addHandler("MerchantDeregisterRequested",this::deleteMerchant);
            this.queue.addHandler("BankAccountNumbersRequest", this::getBankAccountNumbers);
        }
    }

    /**
     * @author s204166
     */
    public Account createCustomer(Event event) {
        Account person = event.getArgument(0, Account.class);
        UUID correlation = event.getArgument(1, UUID.class);
        person.setId(UUID.randomUUID());
        customers.add(person);
        Event result = new Event("CustomerRegistered", new Object[] { person, correlation });
        queue.publish(result);
        return person;
    }

    /**
     * @author s204166
     */
    public void deleteCustomer(Event event) {
        UUID customerId = event.getArgument(0, UUID.class);
        this.customers = this.customers.stream()
                .filter(c -> !c.getId().equals(customerId))
                .collect(Collectors.toList());
    }

    /**
     * @author s204166
     */
    public Account createMerchant(Event event) {
        Account person = event.getArgument(0, Account.class);
        UUID correlation = event.getArgument(1, UUID.class);
        person.setId(UUID.randomUUID());
        merchants.add(person);
        Event result = new Event("MerchantRegistered", new Object[] { person, correlation });
        queue.publish(result);
        return person;
    }

    /**
     * @author s204197
     */
    public void deleteMerchant(Event event) {
        UUID merchantId = event.getArgument(0, UUID.class);

        this.merchants = this.merchants.stream()
                .filter(c -> !c.getId().equals(merchantId))
                .collect(Collectors.toList());

    }

    /**
     * @author s204197
     */
    public List<String> getBankAccountNumbers(Event event) {
        UUID customerId = event.getArgument(0, UUID.class);
        UUID merchantId = event.getArgument(1, UUID.class);
        UUID correlation = event.getArgument(2, UUID.class);
        List<String> result = new ArrayList<>();

        for(Account customer : customers) {
            if(customer.getId().equals(customerId)) {
                result.add(customer.getBankAccountNumber());
                break;
            }
        }
        for(Account merchant : merchants) {
            if(merchant.getId().equals(merchantId)) {
                result.add(merchant.getBankAccountNumber());
                break;
            }
        }
        if(result.size() != 2) {
            result = null;
        }
        Event response = new Event("BankAccountNumbersResponse", new Object[]{ result, correlation });
        queue.publish(response);
        return result;
    }

    public List<Account> getCustomers() {
        return customers;
    }

    public List<Account> getMerchants() {
        return merchants;
    }
}
