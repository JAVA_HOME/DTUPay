Feature: Account Service feature
  Scenario: A customer registers
    Given customer is not registered
    When an event "CustomerRegistrationRequested" is sent by customer
    Then a new event "CustomerRegistered" is sent and then the customer is registered

  Scenario: A merchant registers
    Given merchant is not registered
    When an event "MerchantRegistrationRequested" is sent by merchant
    Then a new event "MerchantRegistered" is sent and then the merchant is registered

  Scenario: A customer gets deregistered
    Given user is registered
    When an event "CustomerDeregisterRequested" is sent by the customer
    Then the customer is deregistered

  Scenario: A merchant gets deregistered
    Given user is registered
    When an event "MerchantDeregisterRequested" is sent by the merchant
    Then the merchant is deregistered

  Scenario: The bankIds are shown
    Given there is a customer and a merchant in the system
    When an event "BankAccountNumbersRequest" is sent by the system
    Then a new event "BankAccountNumbersResponse" is returned