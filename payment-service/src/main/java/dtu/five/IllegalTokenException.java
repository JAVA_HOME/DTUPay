package dtu.five;

public class IllegalTokenException extends Exception {
    public IllegalTokenException(String message) {
        super(message);
    }
}
