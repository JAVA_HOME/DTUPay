package dtu.five;


import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceService;
import messaging.implementations.RabbitMqQueue;

public class StartUp {
    public static void main(String[] args) throws Exception {
        new StartUp().startUp();
    }

    private void startUp() throws Exception {
        RabbitMqQueue mq = new RabbitMqQueue("rabbitMq");
        BankService bankService = new BankServiceService().getBankServicePort();
        new PaymentService(mq, bankService);
    }
}
