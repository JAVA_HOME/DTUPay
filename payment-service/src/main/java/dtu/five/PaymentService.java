package dtu.five;

import dtu.five.dto.Token;
import dtu.five.dto.Transaction;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import messaging.Event;
import messaging.MessageQueue;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

public class PaymentService {

    private BankService bankService;
    private MessageQueue queue;

    private Map<UUID, CompletableFuture<UUID>> idsFromTokens = new ConcurrentHashMap<>();
    private Map<UUID, CompletableFuture<List<String>>> receivedBankAccountNumbers = new ConcurrentHashMap<>();

    /**
     * @author 204423
     */
    public PaymentService(MessageQueue queue, BankService bankService) {
        this.bankService = bankService;
        this.queue = queue;

        synchronized (this) {
            this.queue.addHandler("PaymentRequested", this::paymentHandler);
            this.queue.addHandler("IdFromTokenResponse", this::idFromTokenResponse);
            this.queue.addHandler("BankAccountNumbersResponse", this::bankAccountNumbersReponse);
        }
    }

    /**
     * @author s183816
     */
    private synchronized UUID getCustomerIdFromToken(Token token, UUID correlation) throws IllegalTokenException {
        idsFromTokens.put(correlation, new CompletableFuture<>());
        Event tokenEvent = new Event("IdFromTokenRequest", new Object[]{ token, correlation });
        queue.publish(tokenEvent);
        UUID customerId = idsFromTokens.get(correlation).join();

        boolean isTokenUsed = (customerId == null);
        if (isTokenUsed) {
            throw new IllegalTokenException("Token is already used and thus no longer valid");
        }

        return customerId;
    }

    /**
     * @author s183816
     */
    private synchronized List<String> getBankAccountNumbersFromIds(
            UUID customerId
            , UUID merchantId
            , UUID correlation
    ) {
        receivedBankAccountNumbers.put(correlation, new CompletableFuture<>());
        Event bankAccountEvent = new Event(
                "BankAccountNumbersRequest"
                , new Object[]{ customerId, merchantId, correlation }
        );
        queue.publish(bankAccountEvent);
        return receivedBankAccountNumbers.get(correlation).join();
    }

    /**
     * @author s183816
     */
    private void processTransaction(
            UUID customerId
            , UUID merchantId
            , int amount
            , UUID correlation
    ) throws BankServiceException_Exception {
        List<String> bankAccountNumbers =
                this.getBankAccountNumbersFromIds(customerId, merchantId, correlation);
        String customerBankAccountNumber = bankAccountNumbers.get(0);
        String merchantBankAccountNumber = bankAccountNumbers.get(1);
        bankService.transferMoneyFromTo(
                customerBankAccountNumber
                , merchantBankAccountNumber
                , new BigDecimal(amount)
                , customerBankAccountNumber + " sends " + amount + " to " + merchantBankAccountNumber
        );
    }

    /**
     * @author s204423
     */
    public void paymentHandler(Event event) {
        Transaction transaction = event.getArgument(0,Transaction.class);
        UUID correlation = event.getArgument(1, UUID.class);

        UUID customerId;
        try {
            customerId = this.getCustomerIdFromToken(transaction.getToken(), correlation);
            UUID merchantId = transaction.getMid();
            int amount = transaction.getAmount();
            processTransaction(customerId, merchantId, amount, correlation);
        } catch(BankServiceException_Exception | IllegalTokenException e) {
            queue.publish(new Event("PaymentFailed", new Object[]{ correlation }));
            return;
        }

        Event success =  new Event(
                "SuccessfulPayment"
                , new Object[]{ customerId, transaction, correlation }
        );
        queue.publish(success);
    }

    /**
     * @author s204452
     */
    public void idFromTokenResponse(Event event) {
        UUID customerId = event.getArgument(0, UUID.class);
        UUID correlation = event.getArgument(1, UUID.class);
        idsFromTokens.get(correlation).complete(customerId);
    }

    /**
     * @author s204452
     */
    public void bankAccountNumbersReponse(Event event) {
        List<String> response = event.getArgument(0, List.class);
        UUID correlation = event.getArgument(1, UUID.class);
        receivedBankAccountNumbers.get(correlation).complete(response);
    }
}
