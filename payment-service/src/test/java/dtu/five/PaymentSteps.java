package dtu.five;

import dtu.five.dto.Token;
import dtu.five.dto.Transaction;
import dtu.ws.fastmoney.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.*;

/**
 * @author s183816
 */
public class PaymentSteps {
    private BankService bankService = new MockBankService();
    private MessageQueue messageQueue = mock(MessageQueue.class);
    private PaymentService paymentService = new PaymentService(this.messageQueue, this.bankService);

    // Delay (milliseconds) in Mockito verify to account for concurrency.
    private final int VERIFY_TIMEOUT_MS = 1000;

    private UUID correlation;

    private UUID customerId;
    private UUID merchantId;
    private Transaction transaction;
    private Token token;

    private int customerBankAccountBalance;
    private final int MERCHANT_ACCOUNT_BALANCE = 1000;

    /**
     * @author s183816
     */
    @Given("a transaction containing a merchant ID, a token, and an amount of {int} kr")
    public void aTransactionContainingAMerchantIDATokenAndAnAmountOfKr(int amount) {
        this.merchantId = UUID.randomUUID();
        this.token = new Token();
        this.transaction = new Transaction(this.token, this.merchantId, amount);
    }

    /**
     * @author s183816
     */
    @And("the token belongs to a customer with {int} kr in their bank account")
    public void theTokenBelongsToACustomerWithKrInTheirBankAccount(int bankAccountBalance) {
        this.customerBankAccountBalance = bankAccountBalance;
    }

    /**
     * @author s183816
     */
    @When("a {string} event for a transaction is received")
    public void aPaymentRequestedEventForATransactionIsReceived(String eventName) {
        this.correlation = UUID.randomUUID();
        Event event = new Event(
                eventName
                , new Object[]{ this.transaction, this.correlation}
        );
        new Thread(() -> {
            this.paymentService.paymentHandler(event);
        }).start();
    }

    /**
     * @author s183816
     */
    @Then("an {string} event containing the token is sent")
    public void anEventContainingTheTokenIsSent(String eventName) {
        Event expectedEvent = new Event(
                eventName
                , new Object[]{ this.token, this.correlation }
        );
        verify(this.messageQueue, timeout(this.VERIFY_TIMEOUT_MS)).publish(expectedEvent);
    }

    /**
     * @author s183816
     */
    @When("a {string} event for a customer ID is received")
    public void aEventForACustomerIDIsReceived(String eventName) {
        this.customerId = UUID.randomUUID();
        Event event = new Event(
                eventName,
                new Object[]{ this.customerId, this.correlation }
        );
        this.paymentService.idFromTokenResponse(event);
    }

    /**
     * @author s183816
     */
    @Then("a {string} event containing customer and merchant ids is sent")
    public void aEventContainingCustomerAndMerchantIdsIsSent(String eventName) {
        Event expectedEvent = new Event(
                eventName,
                new Object[]{ this.customerId, this.merchantId, this.correlation }
        );
        verify(this.messageQueue, timeout(this.VERIFY_TIMEOUT_MS)).publish(expectedEvent);
    }

    /**
     * @author s183816
     */
    @When("a {string} event for a list containing the bank account numbers of the customer and merchant")
    public void aEventForAListContainingTheBankAccountNumbersOfTheCustomerAndMerchant(String eventName) throws BankServiceException_Exception {
        User customerUser = new User();
        String customerBankAccountNumber = this.bankService.createAccountWithBalance(
                customerUser,
                BigDecimal.valueOf(this.customerBankAccountBalance)
        );
        User merchantUser = new User();
        String merchantBankAccountNumber = this.bankService.createAccountWithBalance(
                merchantUser,
                BigDecimal.valueOf(this.MERCHANT_ACCOUNT_BALANCE)
        );

        List<String> bankAccountNumbers = List.of(
                customerBankAccountNumber,
                merchantBankAccountNumber
        );

        Event event = new Event(
                eventName,
                new Object[]{ bankAccountNumbers, this.correlation }
        );
        this.paymentService.bankAccountNumbersReponse(event);
    }

    /**
     * @author s183816
     */
    @Then("a {string} event containing customer ID and transaction is sent")
    public void aEventContainingCustomerIDAndTransactionIsSent(String eventName) {
        Event expectedEvent = new Event(
                eventName,
                new Object[]{ this.customerId, this.transaction, this.correlation }
        );
        verify(this.messageQueue, timeout(this.VERIFY_TIMEOUT_MS)).publish(expectedEvent);
    }

    /**
     * @author s183816
     */
    @Then("a {string} event is sent")
    public void aEventIsSent(String eventName) {
        Event expectedEvent = new Event(
                eventName,
                new Object[]{ this.correlation }
        );
        verify(this.messageQueue, timeout(this.VERIFY_TIMEOUT_MS)).publish(expectedEvent);
    }
}
