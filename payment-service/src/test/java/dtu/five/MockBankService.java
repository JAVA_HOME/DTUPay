package dtu.five;

import dtu.ws.fastmoney.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author s183816
 */
public class MockBankService implements BankService {
    Map<String, BigDecimal> userBalances = new HashMap<>();

    @Override
    public Account getAccount(String accountId) throws BankServiceException_Exception {
        return null;
    }

    /**
     * @author s183816
     */
    @Override
    public String createAccountWithBalance(User user, BigDecimal balance) throws BankServiceException_Exception {
        String bankAccountId = UUID.randomUUID().toString();
        userBalances.put(bankAccountId, balance);
        return bankAccountId;
    }

    @Override
    public void retireAccount(String accountId) throws BankServiceException_Exception {

    }

    @Override
    public List<AccountInfo> getAccounts() {
        return null;
    }

    /**
     * @author s183816
     */
    @Override
    public void transferMoneyFromTo(String debtor, String creditor, BigDecimal amount, String description) throws BankServiceException_Exception {
        BigDecimal debtorBalance = userBalances.get(debtor);
        if (debtorBalance.compareTo(amount) < 0) {
            throw new BankServiceException_Exception("Debtor has insufficient fonds", null);
        }
    }
}
