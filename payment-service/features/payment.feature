Feature: Payment service tests

  # @author s183816
  Scenario: Successful payment.
    Given a transaction containing a merchant ID, a token, and an amount of 100 kr
    And the token belongs to a customer with 200 kr in their bank account
    When a "PaymentRequested" event for a transaction is received
    Then an "IdFromTokenRequest" event containing the token is sent
    When a "IdFromTokenResponse" event for a customer ID is received
    Then a "BankAccountNumbersRequest" event containing customer and merchant ids is sent
    When a "BankAccountNumbersResponse" event for a list containing the bank account numbers of the customer and merchant
    Then a "SuccessfulPayment" event containing customer ID and transaction is sent

  # @author s183816
  Scenario: The customer has insufficient fonds
    Given a transaction containing a merchant ID, a token, and an amount of 100 kr
    And the token belongs to a customer with 50 kr in their bank account
    When a "PaymentRequested" event for a transaction is received
    Then an "IdFromTokenRequest" event containing the token is sent
    When a "IdFromTokenResponse" event for a customer ID is received
    Then a "BankAccountNumbersRequest" event containing customer and merchant ids is sent
    When a "BankAccountNumbersResponse" event for a list containing the bank account numbers of the customer and merchant
    Then a "PaymentFailed" event is sent