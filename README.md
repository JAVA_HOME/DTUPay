# 02267 Exam Project: DTU Pay Group 05

| Package | Maven | Java | Quarkus | IntelliJ | Cucumber | JUnit | Gson  | Lombok   |
|---------|-------|------|---------|----------|----------|-------|-------|----------|
| Version | 3.8.7 | 11   | 2.15.1  | 2022.3.1 | 6.9.1    | 5.7.0 | 2.8.5 | 1.18.24  |

## About
This project, is about a payment app, between merchants and customers. The flow is, a customer give one of this RFID tokens to the merchants, that then can start a payment, calling the bank that will then do the transaction.

The project uses **REST** to communicate with the **API**, and **RabbitMQ** for communication between microservices, and the **SOAP** to communicate with the bank, provided by the professor. 

## Quick start
In root directory, there is a script build_and_run.sh which will run all test, and start all docker containers.
> ./build_and_run.sh

## Installation Guide

See installation file.
