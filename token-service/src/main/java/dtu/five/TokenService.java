package dtu.five;

import dtu.five.dto.Account;
import dtu.five.dto.Token;
import dtu.five.dto.Transaction;
import messaging.Event;
import messaging.MessageQueue;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class TokenService {
    public Map<UUID, ArrayList<Token>> tokenDatabase;

    private MessageQueue queue;

    /**
     *
     * @author s204423
     */
    public TokenService(MessageQueue queue) {
        this.queue = queue;
        this.tokenDatabase = new ConcurrentHashMap<UUID, ArrayList<Token>>();

        synchronized (this) {
            this.queue.addHandler("CustomerRegistered", this::createCustomer);
            this.queue.addHandler("TokenCreationRequested", this::createTokens);
            this.queue.addHandler("IdFromTokenRequest", this::idFromToken);
            this.queue.addHandler("SuccessfulPayment",this::paymentSuccessful);
        }
    }

    /**
     *
     * @author s204452
     */
    public void createCustomer(Event event) {
        UUID cid = event.getArgument(0, Account.class).getId();
        if(!tokenDatabase.containsKey(cid)){
            tokenDatabase.put(cid,new ArrayList<Token>());
        }
    }

    /**
     *
     * @author s204452
     */
    public void createTokens(Event event) {
        int amount = event.getArgument(0, Integer.class);
        UUID customerId = event.getArgument(1, UUID.class);
        UUID correlation = event.getArgument(2, UUID.class);
        ArrayList<Token> tokens = tokenDatabase.get(customerId) == null ? new ArrayList<>() : tokenDatabase.get(customerId);
        if (
                !(tokens.size() > 1
                || amount < 1
                || amount > 5)) {
            for (int i = 0; i < amount; i++) {
                tokens.add(new Token());
            }
            tokenDatabase.replace(customerId, tokens);
        }
        Event response = new Event("GetTokenResponse", new Object[]{tokens, correlation});
        queue.publish(response);
    }

    /**
     *
     * @author s204423
     */
    public void idFromToken(Event event) {
        UUID result = null;
        Token token = event.getArgument(0, Token.class);
        UUID correlation = event.getArgument(1, UUID.class);

        for(Map.Entry<UUID, ArrayList<Token>> entry : tokenDatabase.entrySet()) {
            if(entry.getValue().contains(token)) {
                result = entry.getKey();
            }
        }

        Event response = new Event("IdFromTokenResponse", new Object[]{result, correlation});
        queue.publish(response);
    }

    /**
     *
     * @author s204452
     */
    public void paymentSuccessful(Event event) {
        UUID customerId = event.getArgument(0, UUID.class);
        Transaction transaction = event.getArgument(1, Transaction.class);
        ArrayList<Token> tokens = tokenDatabase.get(customerId);
        tokens.remove(transaction.getToken());
        tokenDatabase.replace(customerId, tokens);
    }
}
