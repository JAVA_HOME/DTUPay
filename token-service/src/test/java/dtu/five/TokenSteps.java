package dtu.five;

import dtu.five.dto.Token;
import dtu.five.dto.Transaction;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;

import java.util.ArrayList;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TokenSteps {

    MessageQueue queue;
    TokenService tokenService;
    UUID correlation;
    Token token;
    UUID customerId;

    /**
     * @author s204452
     */
    public TokenSteps() {
        queue = mock(MessageQueue.class);
        tokenService = new TokenService(queue);
    }

    /**
     * @author s204452
     */
    public void setUpTokenDatabase() {
        customerId = UUID.randomUUID();
        token = new Token();

        tokenService.tokenDatabase.put(customerId, new ArrayList<>() {{
            add(token);
        }});
    }

    /**
     * @author s204452
     */
    @Given("a token exist in the tokenDatabase")
    public void aTokenExistInTheTokenDatabase() {
        setUpTokenDatabase();
    }

    /**
     * @author s204452
     */
    @When("a {string} event for a customer is received")
    public void a_event_for_a_customer_is_received(String type) {
        Event event = new Event(type, new Object[] {token, correlation});
        tokenService.idFromToken(event);
    }

    /**
     * @author s204452
     */
    @Then("the {string} event is sent")
    public void the_event_is_sent(String type) {
        Event event = new Event(type, new Object[] {customerId, correlation});
        verify(queue).publish(event);
    }

    /**
     * @author s204452
     */
    @When("a {string} is received")
    public void aIsReceived(String type) {
        Transaction transaction = new Transaction(token, UUID.randomUUID(), 2);
        Event event = new Event(type, new Object[] {customerId, transaction});
        tokenService.paymentSuccessful(event);
    }

    /**
     * @author s204452
     */
    @Then("the token is removed from the tokenDatabase")
    public void theTokenIsRemovedFromTheTokenDatabase() {
        assertFalse(tokenService.tokenDatabase.get(customerId).contains(token));
    }
}
