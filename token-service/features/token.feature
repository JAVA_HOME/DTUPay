
# @author s204452
Feature: Token
  Scenario: Get id belonging to token
    Given a token exist in the tokenDatabase
    When a "IdFromTokenRequest" event for a customer is received
    Then the "IdFromTokenResponse" event is sent

  Scenario: Token is removed on payment successful
    Given a token exist in the tokenDatabase
    When a "SuccessfulPayment" is received
    Then the token is removed from the tokenDatabase


