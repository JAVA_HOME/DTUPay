package messaging;

import java.util.function.Consumer;

public interface MessageQueue {
	/**
	 * @author HUBERT
	 */

	void publish(Event message);
	void addHandler(String eventType, Consumer<Event> handler);

}
