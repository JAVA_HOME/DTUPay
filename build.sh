#!/bin/bash
set -e

pushd messaging-utilities
./build.sh
popd

# Build the services
pushd account-management-service
./build.sh
popd

pushd payment-service
./build.sh
popd

pushd report-service
./build.sh
popd

pushd token-service
./build.sh
popd

pushd dtu-pay
./build.sh
popd


